import React, { Component } from 'react';

import Counter from './containers/Counter/Counter';
import './App.css';
import LoginPage from './components/login';
import { BrowserRouter, Route } from 'react-router-dom';
import allReducers from './components/reducers/allReducersindex';
import { createStore } from 'redux';
import loggedReducer from './components/reducers/loginReducer';
// import { Provider } from 'redux';
import { Provider } from 'react-redux';
import updateNameList from './components/Actions/updateName';



const store = createStore(
  allReducers, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);


class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <BrowserRouter>
          <div className="mainPage">
            <Route exact path="/" component={LoginPage} />
            <Route path="/About" component={Counter} />
            <div className="App">
              {/* <Counter /> */}


            </div>
          </div>
        </BrowserRouter>
      </Provider>

    );
  }
}

export default App;
