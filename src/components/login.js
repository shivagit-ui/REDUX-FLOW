import React from 'react';
import { Link } from 'react-router-dom';
// import './CounterOutput.css';
import { useSelector, useDispatch } from 'react-redux';
import adddLogin from './Actions/loginAction';
import updateNameList from './Actions/updateName';



function LoginPage() {
    const counter = useSelector(state => state.counter);
    const isLoggedIn = useSelector(state => state.loggedReducer);
    const nameChange = useSelector(state => state.nameReducer);
    const dispatch = useDispatch();
    return (

        <div>
            hey {counter} {isLoggedIn}

            <button onClick={() => dispatch(updateNameList())}>update Name</button>
            <button onClick={() => dispatch(adddLogin())}>click Me</button>

            name Chnage Value {nameChange}
            {isLoggedIn ? <form>
                <div className="form-group row">
                    <label for="inputEmail3" className="col-sm-2 col-form-label">Email</label>
                    <div className="col-sm-5">
                        <input type="text" className="form-control" id="inputEmail3" placeholder="Email" />
                    </div>
                </div>
                <div className="form-group row">
                    <label for="inputPassword3" className="col-sm-2 col-form-label">Password</label>
                    <div className="col-sm-5">
                        <input type="password" className="form-control" id="inputPassword3" placeholder="Password" />
                    </div>
                </div>
                <div className="form-group row">
                    <div className="col-sm-5">
                        <Link to="/About"> <button type="submit" className="btn btn-primary">Sign in</button></Link>

                    </div>
                </div>
            </form> : <h2>No Data Availble</h2>}

        </div>
    );
}
export default LoginPage;