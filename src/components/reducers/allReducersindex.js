import loggedReducer from "./loginReducer";
import counter from "./counterReducer";
import { combineReducers } from "redux";
import nameReducer from "./updateNamesReducer";


const allReducers = combineReducers({
    counter: counter,
    loggedReducer: loggedReducer,
    nameReducer: nameReducer
})



export default allReducers;